# Description:
#   Helps you decide where to go for coffee
#
# Configuration:
#   None
#
# Commands:
#   hubot where should we go for coffee?
#
# Author:
#   Ben and Carl

cafes = [
  "Hollywood",
  "Gasket",
  "Coffee Club",
  "Blues"
]

module.exports = (robot) ->
  robot.respond /where should we go for coffee/i, (msg) ->
    msg.reply "You should go to #{msg.random cafes}"
