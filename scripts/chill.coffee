# Description
#   Chillest monkey
#
# Dependencies:
#   None
#
# Configuration:
#   None
#
# Commands:
#   chill
#
# Author:
#   Unknown

module.exports = (robot) ->

  robot.hear /chill/i, (msg) ->
    msg.send('http://www.chillestmonkey.com/img/monkey.gif')
