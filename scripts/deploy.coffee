# Description
#   Deploy stuff
#
# Dependencies:
#   "child_process": "???"
#   "<module name>": "<module version>"
#
# Configuration:
#   LIST_OF_ENV_VARS_TO_SET
#   fabric script directory
#   list of users allowed to deploy
#
# Commands:
#   hubot deploy to dev - deploys develop branch to the dev environment
#
# Notes:
#   requires python-dev, pip, fabric
#
# Author:
#   carlosfunk

{spawn, exec} = require 'child_process'
module.exports = (robot) ->
  robot.respond /deploy to (dev|develop)/i, (msg) ->
    user = msg.message.user
    console.log 'deploy: deploying to dev'
    msg.send "Ok #{user.name} deploying to dev"

    environment = 'dev'
    branch = 'develop'
    deployCommand = "deploy:target=#{environment},branch=#{branch}"

    ls = spawn 'fab', [deployCommand], cwd: '/home/hubot'
    ls.stdout.on 'data', (data) -> console.log 'stdout: ' + data
    ls.stderr.on 'data', (data) -> console.log 'stderr: ' + data
    ls.on 'close', (code) ->
      if code
        console.log 'child process exited with code ' + code
        msg.send "Sorry, something has gone wrong..."
      else
        msg.send "Success: #{branch} branch deployed to #{environment}"
